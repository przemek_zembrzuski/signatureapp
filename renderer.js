const signatureUpdate = require('./modules/signatureUpdate.js');

const rootPath = 'C:/stopka';
const input = document.querySelector('#capitalInput');
const button = document.querySelector('#container > button');
const showEmoji = (type)=>{
    const emoji = document.querySelector(`#${type}`);
    emoji.classList.add('visible');
    setTimeout(()=>{
        emoji.classList.remove('visible');
    },3000)
}
button.addEventListener('click',()=>{
    if(input.value && !isNaN(input.value)){
        signatureUpdate(input.value,rootPath,(err)=>{
            if(err){
                console.log(err)
                showEmoji('sad')
            }
            showEmoji('happy');
            input.value = ""
        })
    }
})